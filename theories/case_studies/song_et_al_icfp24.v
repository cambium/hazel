(* song_et_al_icfp24.v *)

(* Verification of the program [multi_shot] from the paper "Specification and
   Verification for Unrestricted Algebraic Effects and Handling" (ICFP'24) by
   Yahui Song, Darius Foo, and Wei-Ngan Chin.
*)

From iris.proofmode Require Import base tactics classes.
From iris.algebra Require Import excl_auth agree gset gmap agree csum.
From hazel.program_logic Require Import reasoning_rules.


(* ========================================================================== *)
(** * Mathematical Preliminaries. *)

(* -------------------------------------------------------------------------- *)
(** Definitions. *)

Inductive formula (A B : Type) : Type :=
  | closed (b : B) : formula A B
  | open (F : A → formula A B) : formula A B.

Arguments closed {A B} _.
Arguments open {A B} _.

Definition eval {A B : Type} : list A → formula A B → option B :=
  fix eval rs F {struct F} : option B :=
    match F with
    | closed b =>
        Some b
    | open F =>
        match rs with
        | [] =>
            None
        | r :: rs =>
            eval rs (F r)
        end
    end.

Definition eval_with_default {A B : Type} : A → list A → formula A B → B :=
  fix eval_wd d rs F {struct F} : B :=
    match F with
    | closed b =>
        b
    | open F =>
        match rs with
        | [] =>
            eval_wd d [] (F d)
        | r :: rs =>
            eval_wd d rs (F r)
        end
    end.


(* -------------------------------------------------------------------------- *)
(** Properties. *)

Section properties.
Context {A B : Type}.
Implicit Types F : formula A B.

Lemma eval_eval_with_default d rs F b :
  eval rs F = Some b →
    eval_with_default d rs F = b.
Proof.
  revert b rs.
  induction F as [|F IH]; intros b' rs; simpl.
  - by inversion 1.
  - case rs as [|r rs].
    + by inversion 1.
    + by apply IH.
Qed.

Lemma eval_with_default_snoc d rs F :
  eval_with_default d (rs ++ [d]) F =
  eval_with_default d rs F.
Proof.
  revert rs.
  induction F as [|F IH]; simpl; [done|].
  case rs as [| r rs]; simpl; [done|].
  by apply IH.
Qed.
End properties.


(* ========================================================================== *)
(** * Implementation. *)

Section implementation.

  Definition assert_ := (λ: "b",
    if: "b" then #() else (#() #())
  )%V.

  Definition callee : val := (λ: <>,
    let: "x" := ref #0%Z in
    let: "ret" := doₘ: #() in
    "x" <- (Load "x") + #1%Z;;
    assert_ (#1%Z ≤ Load "x");;
    "ret" + #2%Z
  )%V.

  Definition multi_shot : val := (λ: <>,
    deep-try: callee #() with
      effect λ: <> "k", "k" #4%Z;; "k" #5%Z
    | return λ: "b", "b"
    end
  )%V.

End implementation.


(* ========================================================================== *)
(** * Specification. *)

Class multi_shotG Σ := {
  lookupG :: inG Σ (excl_authR (listO ZO));
  oneshotG :: inG Σ (csumR (exclR unitO) (agreeR locO));
}.
Definition multi_shotΣ := #[
  GFunctor (excl_authR (listO ZO));
  GFunctor (csumR (exclR unitO) (agreeR locO))
].
Instance subG_multi_shotΣ {Σ} : subG multi_shotΣ Σ → multi_shotG Σ.
Proof. solve_inG. Qed.

Section specification.
  Context `{!heapGS Σ} `{!multi_shotG Σ}.

  Definition clientView replies (rs : list Z) :=
    own replies (◯E (rs : ofe_car (listO ZO))).
  Definition handlerView replies (rs : list Z) :=
    own replies (●E (rs : ofe_car (listO ZO))).

  Definition pending loc := own loc (Cinl (Excl (tt : unitO))).
  Definition shot loc (x : locations.loc) := own loc (Cinr (to_agree (x : locO))).

  Definition LOOKUP local_res replies : iEff Σ :=
    (>> rs      >> ! #() {{ local_res ∗ clientView replies rs          }};
     << (r : Z) << ? #r  {{ local_res ∗ clientView replies (rs ++ [r]) }} @ MS)%ieff.

  Definition local_res loc : iProp Σ :=
    ∃ (x : locations.loc) (a : Z), x ↦ #a ∗ shot loc x ∗ ⌜ (0 ≤ a)%Z ⌝.

  Definition callee_spec loc replies :=
    ∀ rs,
    pending loc -∗
    clientView replies rs -∗
    EWP callee #() .{| LOOKUP (local_res loc) replies |} {{ y, ∃ rs' (a : Z),
      local_res loc ∗
      clientView replies (rs ++ rs') ∗
      ⌜ eval rs' (open (λ r, closed (r + 2)%Z)) = Some a ⌝ ∗
      ⌜ y = #a%Z ⌝ }}.

  Definition multi_shot_spec :=
    ⊢ EWP multi_shot #() .{| ⊥ |} {{ y,
        ⌜ y = #7%Z ⌝ }}.

End specification.


(* ========================================================================== *)
(** * Verification. *)

(* -------------------------------------------------------------------------- *)
(** Ghost theory. *)

Section ghost_theory.
  Context `{!multi_shotG Σ}.

  Lemma introduce_list rs :
    ⊢ (|==> ∃ replies, handlerView replies rs ∗ clientView replies rs)%I.
  Proof.
    iMod (own_alloc ((●E (rs : ofe_car (listO _))) ⋅
                     (◯E (rs : ofe_car (listO _))))) as (γ) "[??]";
    [ apply excl_auth_valid | eauto with iFrame ]; done.
  Qed.

  Lemma check_list replies rs rs' :
    handlerView replies rs -∗ clientView replies rs' -∗ ⌜ rs = rs' ⌝.
  Proof.
    iIntros "H● H◯".
    by iDestruct (own_valid_2 with "H● H◯") as %?%excl_auth_agree_L.
  Qed.

  Lemma update_list replies rs'' rs rs' :
    handlerView replies rs -∗
    clientView replies rs' ==∗
      handlerView replies rs'' ∗ clientView replies rs''.
  Proof.
    iIntros "Hγ● Hγ◯".
    iMod (own_update_2 _ _ _ (●E (rs'' : ofe_car (listO _)) ⋅
                              ◯E (rs'' : ofe_car (listO _)))
      with "Hγ● Hγ◯") as "[$$]";
    [ apply excl_auth_update | ]; done.
  Qed.

  Lemma shooting_permission :
    ⊢ (|==> ∃ loc, pending loc)%I.
  Proof.
    iMod (own_alloc (Cinl (Excl (tt : ofe_car unitO)))) as (γ) "?";
    [ done | by eauto with iFrame ].
  Qed.

  Lemma shoot loc x : pending loc ==∗ shot loc x.
  Proof.
    iIntros "Hloc".
    iMod (own_update loc _ (Cinr (to_agree (x : ofe_car locO)))
      with "Hloc") as "$"; [|done].
    intros n mf Hm.
    destruct mf as [[a'|b'|]|]; try by destruct Hm.
  Qed.

  Lemma shot_agree loc x x' : shot loc x -∗ shot loc x' -∗ ⌜ x = x' ⌝.
  Proof.
    iIntros "Hloc Hloc'".
    iDestruct (own_valid_2 with "Hloc Hloc'") as %Hvalid.
    iPureIntro. revert Hvalid.
    rewrite -Cinr_op Cinr_valid.
    by apply to_agree_op_inv_L.
  Qed.

End ghost_theory.


(* -------------------------------------------------------------------------- *)
(** Rewriting principle for [LOOKUP]. *)

Lemma upcl_LOOKUP `{!heapGS Σ, !multi_shotG Σ} local_res replies v Φ :
  iEff_car (Σ:=Σ) (upcl MS (LOOKUP local_res replies)) v Φ ≡
    (∃ rs, ⌜ v = #() ⌝ ∗
      (local_res ∗ (clientView replies rs)) ∗
      □ ∀ (r : Z), (local_res ∗ (clientView replies (rs ++ [r]))) -∗ Φ #r
    )%I.
Proof. by rewrite /LOOKUP (pers_upcl_tele' [tele _] [tele _]). Qed.


(* -------------------------------------------------------------------------- *)
Section verification.
  Context `{!heapGS Σ, !multi_shotG Σ}.

  Lemma callee_correct loc replies : callee_spec loc replies.
  Proof.
    iIntros (rs) "Hloc Hreplies".
    unfold callee. ewp_pure_steps.
    ewp_bind_rule.
    iApply ewp_alloc.
    iIntros (x) "!> Hx !>". simpl.
    ewp_pure_steps.
    iApply fupd_ewp.
    iMod (shoot loc x with "Hloc") as "#Hloc".
    iModIntro.
    ewp_pure_steps.
    iApply ewp_do_ms.
    rewrite upcl_LOOKUP.
    iExists rs. iFrame. iSplit; [done|].
    iSplitL. { by iSplit. }
    iIntros "!>" (?) "[(%x' & %a & Hx & Hloc' & %Ha) Hreplies]".
    iDestruct (shot_agree with "Hloc Hloc'") as %<-.
    ewp_pure_steps.
    ewp_bind_rule.
    iApply (ewp_load with "Hx").
    iIntros "!> Hx !>". simpl.
    ewp_pure_steps. done.
    ewp_bind_rule.
    iApply (ewp_store with "Hx").
    iIntros "!> Hx !>". simpl.
    ewp_pure_steps.
    ewp_bind_rule.
    iApply (ewp_load with "Hx").
    iIntros "!> Hx !>". simpl.
    ewp_pure_steps. done.
    rewrite bool_decide_eq_true_2; [|lia].
    unfold assert_.
    ewp_pure_steps. done.
    iExists [r], (r + 2%Z)%Z. iFrame.
    iSplitL;[| by iPureIntro].
    by iPureIntro; lia.
  Qed.

  Lemma eval_with_five_correct F local_res replies :
    ∀ rs,
    handlerView replies rs -∗
      deep_handler ⊤
         (* Handlee's protocol pair: *) ⊥ (LOOKUP local_res replies)
        ((* Handlee's postcondition: *) λ y, ∃ rs' (a : Z),
           local_res ∗ clientView replies rs' ∗
           ⌜ eval rs' F = Some a ⌝ ∗ ⌜ y = #a ⌝
        )
        ((* Effect branch: *) λ: <> "k", "k" #4%Z;; "k" #5%Z)%V
        ((* Return branch: *) λ: "b", "b")%V
        ⊥ ⊥
        ((* Handler's postcondition: *) λ a,
           local_res ∗
           handlerView replies rs ∗
           clientView replies rs ∗
           ⌜ a = #(eval_with_default 5 rs F) ⌝).
  Proof.
    iIntros (rs) "Hrs". iLöb as "IH" forall (rs).
    rewrite deep_handler_unfold.
    iSplit; [|iSplit]; [|by iIntros (??) "HFalse"; rewrite upcl_bottom|].
    - iIntros (y) "[%rs' [%a (HR & HclientView & %Heval & ->)]]".
      iApply fupd_ewp.
      iDestruct (check_list with "Hrs HclientView") as %<-.
      iModIntro. ewp_pure_steps. iFrame.
      by rewrite (eval_eval_with_default _ _ _ a).
    - iIntros (v k). rewrite upcl_LOOKUP.
      iIntros "(%rs' & (-> & [HR HclientView] & #Hk))".
      ewp_pure_steps.
      iApply fupd_ewp.
      iDestruct (check_list with "Hrs HclientView") as %<-.
      iMod (update_list _ (rs ++ [4%Z]) with "Hrs HclientView")
        as "[Hrs HclientView]".
      iModIntro.
      ewp_bind_rule.
      iApply (ewp_mono with "[HR HclientView Hrs]").
      { iApply ("Hk" with "[HR HclientView]"). { by iFrame. }
        by iApply ("IH" with "Hrs").
      }
      iIntros (?) "(HR & Hrs & HclientView & ->)". simpl.
      iMod (update_list _ (rs ++ [5%Z]) with "Hrs HclientView")
        as "[Hrs HclientView]".
      iModIntro. ewp_pure_steps.
      iApply (ewp_mono with "[HR HclientView Hrs]").
      { iApply ("Hk" with "[HR HclientView]"). { by iFrame. }
        by iApply ("IH" with "Hrs").
      }
      iIntros (?) "(HR & Hrs & HclientView & ->)".
      iMod (update_list _ rs with "Hrs HclientView")
        as "[Hrs HclientView]".
      iModIntro. iFrame.
      by rewrite eval_with_default_snoc.
  Qed.

  Lemma multi_shot_correct : multi_shot_spec.
  Proof.
    unfold multi_shot_spec, multi_shot, callee.
    iApply fupd_ewp.
    iMod (introduce_list ([] : list Z))
      as "[%replies [HhandlerView HclientView]]".
    iMod shooting_permission as "[%loc Hloc]".
    iModIntro.
    ewp_pure_steps.
    set R := local_res loc.
    iApply (ewp_mono with "[HhandlerView HclientView Hloc]").
    - iApply (ewp_deep_try_with with "[HclientView Hloc]").
      { iApply (ewp_pers_mono _ _ _ _ (λ y , ∃ rs' (a : Z),
          R ∗
          clientView replies rs' ∗
          ⌜ eval rs' _ = Some a ⌝ ∗
          ⌜ y = #a%Z ⌝)
          with "[HclientView Hloc]")%I.
        + by iApply (callee_correct with "Hloc HclientView").
        + iIntros "!#" (?) "[%rs' [%a (?&?&?&?)]] !>".
          iExists rs', a. by iFrame.
      }
      { by iApply (eval_with_five_correct with "HhandlerView"). }
    - iIntros (y) "(_ & _ & _ & ->) !>". by simpl.
  Qed.

End verification.
