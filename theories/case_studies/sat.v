(* sat.v *)

(* This file includes the verification of a simple SAT solver that uses
   multi-shot continuations to implement backtracking. *)

From iris.proofmode Require Import base tactics classes.
From iris.algebra Require Import excl_auth agree gset gmap agree.
From hazel.program_logic Require Import reasoning_rules.
From hazel.case_studies Require Import map_lib.


(* ========================================================================== *)
(** * Mathematical Preliminaries. *)

(* -------------------------------------------------------------------------- *)
(** Definitions. *)

(* A literal is a pair of a sign [b] and an index [i]. *)
Definition literal : Set := bool * nat.

(* Propositional formulas. *)
Inductive formula :=
  | Conj (p₁ p₂ : formula)
  | Disj (p₁ p₂ : formula)
  | Lit  (l     : literal).

(* The function [eval] computes the evaluation of a propositional formula [p]
   under an assignment [M]. It is a partial function: if the formula contains
   unassigned literals, then the result of [eval] is [None]. *)
Definition eval : gmap nat bool → formula → option bool :=
  fix eval M p {struct p} : option bool :=
    match p with
    | Conj p₁ p₂ =>
        eval M p₁ ≫= λ (b : bool),
        if b then eval M p₂ else mret false
    | Disj p₁ p₂ =>
        eval M p₁ ≫= λ (b : bool),
        if b then mret true else eval M p₂
    | Lit (s, i) =>
        M !! i ≫= λ b,
        mret (eqb s b)
        (* or simply: (eqb s) <$> (M !! i). *)
    end.

(* A formula [p] is _satisfiable_ if there exists an assignment [M],
   such that [eval M p = Some true]. *)
Definition satisfiable' M p := ∃ M', eval (M ∪ M') p = Some true.
Definition satisfiable p := satisfiable' ∅ p.

(* The function [indices] computes the set of indices appearing in
   literals of a given formula [p]. *)
Definition indices : formula → list nat :=
  fix indices p {struct p} :=
    match p with
    | Lit (_, i) =>
        [i]
    | Conj p₁ p₂ | Disj p₁ p₂ =>
        indices p₁ ++ indices p₂
    end.

(* To prove that [satisfiable p] is decidable for every [p],
   we introduce the _decision procedure_ [sat_proc]. *)
Definition sat_proc' (p : formula) : gmap nat bool → list nat → bool :=
  fix sat_proc' M js {struct js} :=
    match js with
    | [] =>
        default false (eval M p)
    | j :: js =>
        sat_proc' (M ∪ {[j:=true ]}) js ||
        sat_proc' (M ∪ {[j:=false]}) js
    end.
Definition sat_proc (p : formula) : bool := sat_proc' p ∅ (indices p).


(* -------------------------------------------------------------------------- *)
(** Properties of [eval]. *)

Lemma eval_conj_true M p₁ p₂ :
  eval M (Conj p₁ p₂) = Some true ↔
    eval M p₁ = Some true ∧ eval M p₂ = Some true.
Proof.
  split; simpl; [|by intros [-> ->]].
  destruct (eval M p₁) as [b1|] eqn:?;
  destruct (eval M p₂) as [b2|] eqn:?;
  simpl; try done; by destruct b1.
Qed.

Lemma eval_conj_false M p₁ p₂ :
  eval M (Conj p₁ p₂) = Some false ↔
    eval M p₁ = Some false ∨ (eval M p₁ = Some true ∧ eval M p₂ = Some false).
Proof.
  split; simpl; [|intros [->|[-> H]]]; try done.
  destruct (eval M p₁) as [b1|] eqn:?;
  destruct (eval M p₂) as [b2|] eqn:?;
  simpl; try done; destruct b1; [right|left|right|left]; done.
Qed.

Lemma eval_disj_true M p₁ p₂ :
  eval M (Disj p₁ p₂) = Some true ↔
    eval M p₁ = Some true ∨ (eval M p₁ = Some false ∧ eval M p₂ = Some true).
Proof.
  split; simpl; [|by intros [->|[-> H]]].
  destruct (eval M p₁) as [b1|] eqn:?;
  destruct (eval M p₂) as [b2|] eqn:?;
  simpl; try done; destruct b1; [left|right|left|right]; done.
Qed.

Lemma eval_disj_false M p₁ p₂ :
  eval M (Disj p₁ p₂) = Some false ↔
    eval M p₁ = Some false ∧ eval M p₂ = Some false.
Proof.
  split; simpl; [|by intros [-> ->]].
  destruct (eval M p₁) as [b1|] eqn:?;
  destruct (eval M p₂) as [b2|] eqn:?;
  simpl; try done; by destruct b1.
Qed.

Lemma eval_lit_Some_iff M l s i b b' :
  l = (s, i) →
    b = eqb s b' →
      eval M (Lit l) = Some b ↔ M !! i = Some b'.
Proof.
  intros -> ->. simpl.
  by split; [destruct (M !! i); [destruct s, b, b'|]|intros ->].
Qed.

Lemma eval_lit_Some_iff' M s i b :
  eval M (Lit (s, i)) = Some b ↔ M !! i = Some (eqb s b).
Proof. by rewrite (eval_lit_Some_iff _ _ s i);[| |destruct s, b]. Qed.

Lemma eval_lit_Some M s i b :
  M !! i = Some b → eval M (Lit (s, i)) = Some (eqb s b).
Proof. simpl. by intros ->. Qed.

Lemma eval_lit_None M s i : M !! i = None → eval M (Lit (s, i)) = None.
Proof. simpl. by intros ->. Qed.

Lemma eval_lit_insert M s i b :
  eval (<[i:=b]> M) (Lit (s, i)) = Some (eqb s b).
Proof. simpl. by rewrite lookup_insert. Qed.

Lemma eval_union M M' p b :
  eval M p = Some b → eval (M ∪ M') p = Some b.
Proof.
  revert b.
  induction p as [p₁ IHp₁ p₂ IHp₂|p₁ IHp₁ p₂ IHp₂|(s, i)]; intros b.
  (* Conj. *)
  { destruct b.
    - rewrite !eval_conj_true; intros [Hp₁ Hp₂];
      by split; [apply IHp₁|apply IHp₂].
    - by rewrite !eval_conj_false; intros [Hp₁|[Hp₁ Hp₂]]; [left|right;split];
         try apply IHp₁; try apply IHp₂.
  }
  (* Disj. *)
  { destruct b.
    - by rewrite !eval_disj_true; intros [Hp₁|[Hp₁ Hp₂]];
         [left|right;split]; try apply IHp₁; try apply IHp₂.
    - rewrite !eval_disj_false; intros [Hp₁ Hp₂];
      by split; [apply IHp₁|apply IHp₂].
  }
  (* Lit. *)
  { rewrite !(eval_lit_Some_iff' _ s i); intro Hlkup;
    by apply lookup_union_Some_l. }
Qed.

Lemma eval_true M p : eval M p = Some true → satisfiable' M p.
Proof. intros Heval. exists ∅. by rewrite map_union_empty. Qed.

Lemma eval_false M p : eval M p = Some false → ¬ satisfiable' M p.
Proof.
  by intros Heval [M' Hsat];
     revert Hsat;
     rewrite (eval_union _ _ _ _ Heval).
Qed.

Lemma eval_restriction M p :
  eval M p = eval (filter (λ '(i, _), i ∈ indices p) M) p.
Proof.
  have Haux: ∀ p (l s : list nat), l ⊆ s →
    (∀ M, eval M p = eval (filter (λ '(i, _), i ∈ l) M) p) →
      (∀ M, eval M p = eval (filter (λ '(i, _), i ∈ s) M) p).
  { clear M p. intros p l s Hls Heval M. rewrite Heval. symmetry.
    rewrite -(map_filter_filter_l (λ '(i, _), i ∈ l) (λ '(i, _), i ∈ s) M);
    [by apply Heval|].
    intros i b _ Hin. by apply Hls.
  }

  revert M. induction p as [| |(s, i)]; intro M; simpl.
  (* Conj. *)
  { rewrite (Haux p1 _ (indices p1 ++ indices p2) _ IHp1); [
    rewrite (Haux p2 _ (indices p1 ++ indices p2) _ IHp2); [done|] |].
    { intros i. rewrite elem_of_app. by right. }
    { intros i. rewrite elem_of_app. by left. }
  }
  (* Disj. *)
  { rewrite (Haux p1 _ (indices p1 ++ indices p2) _ IHp1); [
    rewrite (Haux p2 _ (indices p1 ++ indices p2) _ IHp2); [done|] |].
    { intros i. rewrite elem_of_app. by right. }
    { intros i. rewrite elem_of_app. by left. }
  }
  (* Lit. *)
  { destruct (M !! i) as [b|] eqn:?.
    { by rewrite (map_lookup_filter_Some_2 _ _ _ _ Heqo);
         [|rewrite elem_of_list_singleton ]. }
    { by rewrite (map_lookup_filter_None_2 _ _ i); [|left]. }
  }
Qed.


(* -------------------------------------------------------------------------- *)
(** Properties of [satisfiable] and [sat_proc]. *)

Lemma satisfiable_insert_iff' M i p :
  M !! i = None →
    satisfiable' M p ↔ ∃ b, satisfiable' (<[i:=b]> M) p.
Proof.
  intro Hlkup. split.
  { intros [M' Heval]. destruct (M' !! i) as [b|] eqn:?.
    { exists b, (delete i M').
      rewrite -insert_union_l insert_union_r; [|done].
      by rewrite insert_delete.
    }
    { exists true, M'.
      rewrite (_: <[i:=true]> M ∪ M' = (M ∪ M') ∪ {[i:=true]});
      first by apply eval_union.
      rewrite -insert_union_l insert_union_singleton_r; [done|].
      by rewrite lookup_union_None.
    }
  }
  { intros [b [M' Heval]]. exists (<[i:=b]>M').
    by rewrite -Heval -insert_union_l insert_union_r. }
Qed.

(* [sat_proc' p M js] tells whether [M] can be extended with an assignement
   [M'] of indices in [js] such that [eval (M ∪ M') p] is true. *)
Lemma sat_proc_spec' p M js :
  sat_proc' p M js = true ↔
    ∃ M', elements (dom M') ⊆ js ∧ eval (M ∪ M') p = Some true.
Proof.
  (* Proof by induction on the list of indices [js]. *)
  revert js M. induction js as [|j js]; intro M.
  - (* Case [[]]. *)
    simpl. split.
    + destruct (eval M p) as [b|] eqn:?; simpl; try done.
      intros ->. by exists ∅; split; [|apply eval_union].
    + intros [M' [Hdom Heval]].
      specialize (list_nil_subseteq _ Hdom).
      rewrite elements_empty_iff dom_empty_iff.
      intros ->. revert Heval. rewrite map_union_empty.
      by intros ->.
  - (* Case [j :: js]. *)
    simpl.
    have Haux: (∃ b, sat_proc' p (M ∪ {[j:=b]}) js = true) ↔
      (∃ M', elements (dom M') ⊆ j :: js ∧
             eval (M ∪ M') p = Some true).
    { split; [intros [b Hsat]|intros [M' [Hdom Heval]]].
      { revert Hsat. rewrite IHjs. intros [M' [Hdom Heval]].
        exists ({[j:=b]} ∪ M').
        rewrite map_union_assoc Heval. split; [|done].
        rewrite dom_union. intro i.
        rewrite elem_of_elements elem_of_union dom_singleton
                elem_of_singleton elem_of_cons.
        by intros [->|Hdom']; [left|right];
           [|apply Hdom;rewrite elem_of_elements].
      }
      { exists (default true (M' !! j)).
        rewrite IHjs.
        exists (delete j M'). split.
        { rewrite dom_delete. intro i. specialize (Hdom i). revert Hdom.
          rewrite !elem_of_elements elem_of_difference
                   elem_of_cons elem_of_singleton.
          naive_solver.
        }
        { rewrite -map_union_assoc -insert_union_singleton_l.
          rewrite insert_delete_insert.
          destruct (M' !! j) as [b|] eqn:?; [by rewrite insert_id|].
          simpl. rewrite insert_union_singleton_r; [|done].
          rewrite map_union_assoc.
          by apply eval_union.
        }
      }
    }
    simpl. rewrite -Haux orb_true_iff. split.
    { intros [|]; [exists true|exists false]; done. }
    { intros [b Hb]; destruct b; [left|right]; done. }
Qed.

Lemma decide_sat' M p : sat_proc' p M (indices p) = true ↔ satisfiable' M p.
Proof.
  rewrite sat_proc_spec'. split.
  { intros [M' [_ Heval]]. by exists M'. }
  { intros [M' Heval].
    exists (filter (λ '(i, _), i ∈ indices p) M'). split.
    { intro i. rewrite elem_of_elements elem_of_dom.
      intros [b Hlkup]. revert Hlkup.
      rewrite map_lookup_filter_Some. by intros [_ ?].
    }
    { rewrite -Heval eval_restriction (eval_restriction (M ∪ M')).
      have Haux: ∀ M', filter (λ '(i, _), i ∈ indices p) (M ∪ M') =
        filter (λ '(i, _), i ∈ indices p) M ∪
        filter (λ '(i, _), i ∈ indices p) M'.
      { clear Heval M'. intro M'. apply map_eq. intro i.
        destruct (elem_of_list_dec i (indices p)) as [Hin|Hnot_in]; [
        destruct (M !! i) as [b|] eqn:?; [|
        destruct (M' !! i) as [b'|] eqn:? ] |].
        { transitivity (Some b); [
          by rewrite map_lookup_filter_Some (lookup_union_Some_l _ _ i b)|].
          symmetry.
          rewrite lookup_union_Some_raw map_lookup_filter_Some.
          by left.
        }
        { transitivity (Some b'); [
          by rewrite map_lookup_filter_Some lookup_union_r|].
          symmetry. rewrite lookup_union_Some_raw. right.
          rewrite map_lookup_filter_None map_lookup_filter_Some. by auto.
        }
        { transitivity (@None bool); [
          by rewrite map_lookup_filter_None lookup_union_r; [left|] |].
          symmetry. rewrite lookup_union_None !map_lookup_filter_None.
          split; by left.
        }
        { transitivity (@None bool); [
          rewrite map_lookup_filter_None; by right|].
          symmetry. rewrite lookup_union_None !map_lookup_filter_None.
          split; by right.
        }
      }
      rewrite !Haux. rewrite map_filter_filter_l; [done|]. by auto.
    }
  }
Qed.

Lemma decide_sat p : sat_proc p = true ↔ satisfiable p.
Proof. by apply decide_sat'. Qed.

Global Instance satisfiable_dec' M p : Decision (satisfiable' M p).
Proof.
  destruct (sat_proc' p M (indices p)) as [|] eqn:?.
  { left.  by rewrite -decide_sat'. }
  { right. by rewrite -decide_sat' Heqb. }
Qed.

Global Instance satisfiable_dec p : Decision (satisfiable p).
Proof. by apply satisfiable_dec'. Qed.


(* ========================================================================== *)
(** * Implementation. *)

Notation conj p₁ p₂ := (InjL (InjL (Pair p₁ p₂))) (only parsing).
Notation disj p₁ p₂ := (InjL (InjR (Pair p₁ p₂))) (only parsing).
Notation lit s i := (InjR (Pair s i)) (only parsing).

Notation conj' p₁ p₂ := (InjLV (InjLV (PairV p₁ p₂))) (only parsing).
Notation disj' p₁ p₂ := (InjLV (InjRV (PairV p₁ p₂))) (only parsing).
Notation lit' s i := (InjRV (PairV s i)) (only parsing).

Section implementation.
  Context `{!heapGS Σ}.
  Context `{!MapLib Σ}.

  Definition lit_repr : literal → val := λ '(s, i),
    lit' #(s : bool) #(i : nat).
  Definition form_repr : formula → val :=
    fix form_repr f {struct f} :=
      match f with
      | Conj p₁ p₂ => conj' (form_repr p₁) (form_repr p₂)
      | Disj p₁ p₂ => disj' (form_repr p₁) (form_repr p₂)
      | Lit l => lit_repr l
      end.

  Definition interp_sign : val := (λ: "s" "b",
    if: "s" then "b" else ~ "b"
  )%V.

  Definition interp : val := (
    rec: "interp" "f" :=
      match: "f" with InjL "f" => match: "f" with
        (* Conj: *) InjL "p" => "interp" (Fst "p") && "interp" (Snd "p")
      | (* Disj: *) InjR "p" => "interp" (Fst "p") || "interp" (Snd "p")
          end
      | (* Lit: *) InjR "p" =>  interp_sign (Fst "p") (doₘ: (Snd "p"))
      end
  )%V.

  (* The function [satisfy] indicates whether an input propositional formula [f]
     is _satisfiable_. Its implementation is concise but operationally complex.
   *)
  Definition satisfy : val := (λ: "f",
    let: "m" := make_map #() in
    deep-try: (interp "f") with
      effect (λ: "i" "k",
        match: map_lookup "m" "i" with
          SOME "b" =>
            "k" "b"
        | NONE =>
            (* Resuming the continuation returns a Boolean
               indicating whether [m] can be extended in such a way
               that the evaluation of [f] is true. *)
            (map_insert "m" "i" #true;; "k" #true)
              ||
            (map_insert "m" "i" #false;;
               ("k" #false)
                 ||
               (* If neither [(i, true)] nor [(i, false)] work,
                  then it is impossible to extend [m].
                  However, if further keys are removed from [m],
                  then maybe there is an assignment of [i] for which
                  [f] is true. *)
               (map_delete "m" "i";; #false)
            )
        end
      )
    | return λ: "b",
        (* The Boolean [b] is the evaluation of [f] for
           the current assignment stored in [m]. *)
        "b"
    end
  )%V.

End implementation.


(* ========================================================================== *)
(** * Specification. *)

Section specification.
  Context `{!heapGS Σ}.
  Implicit Types sView : gmap nat bool → iProp Σ. (* The "student view". *)

  (* Describe the lookup request to a map [M], whose state is captured
     by the predicate [sView] and may be updated if the index [i] does
     not belong to [M]. *)
  Definition LOOKUP sView : iEff Σ :=
    (>> M i >> ! #(i : nat)  {{ sView M                    }};
     << b   << ? #(b : bool) {{ ⌜ default b (M !! i) = b ⌝ ∗
                                sView (<[i:=b]> M)         }} @ MS)%ieff.

  Definition interp_spec :=
    ∀ sView M p,
      sView M -∗
        EWP interp (form_repr p) .{| LOOKUP sView |} {{ y, ∃ M' b,
          sView (M ∪ M') ∗ ⌜ eval (M ∪ M') p = Some b ⌝ ∗ ⌜ y = #b ⌝ }}.

  Definition satisfy_spec `{!MapLib Σ} p :=
    ⊢ EWP satisfy (form_repr p) .{| ⊥ |} {{ y,
        ⌜ y = #(bool_decide (satisfiable p)) ⌝ }}.

End specification.


(* ========================================================================== *)
(** * Verification. *)

(* -------------------------------------------------------------------------- *)
(** Ghost theory. *)

(* Camera. *)

Class satG Σ := {
  assignmentG :: inG Σ (excl_authR (gmapO natO boolO));
}.
Definition satΣ := #[ GFunctor (excl_authR (gmapO natO boolO)) ].
Instance subG_interpΣ {Σ} : subG satΣ Σ → satG Σ.
Proof. solve_inG. Qed.


Section ghost_theory.
  Context `{!satG Σ}.

  Definition blackboardSt γ M := own γ (●E (M : ofe_car (gmapO natO boolO))).
  Definition studentView  γ M := own γ (◯E (M : ofe_car (gmapO natO boolO))).

  Lemma introduce_blackboard M :
    ⊢ (|==> ∃ γ, blackboardSt γ M ∗ studentView γ M)%I.
  Proof.
    iMod (own_alloc ((●E (M : ofe_car (gmapO _ _))) ⋅
                     (◯E (M : ofe_car (gmapO _ _))))) as (γ) "[??]";
    [ apply excl_auth_valid | eauto with iFrame ]; done.
  Qed.

  Lemma check_blackboard γ M M' :
    blackboardSt γ M -∗ studentView γ M' -∗ ⌜ M = M' ⌝.
  Proof.
    iIntros "H● H◯".
    by iDestruct (own_valid_2 with "H● H◯") as %?%excl_auth_agree_L.
  Qed.

  Lemma erase_blackboard γ M'' M M' :
    blackboardSt γ M -∗
      studentView γ M' ==∗
        blackboardSt γ M'' ∗ studentView γ M''.
  Proof.
    iIntros "Hγ● Hγ◯".
    iMod (own_update_2 _ _ _ (●E (M'' : ofe_car (gmapO _ _)) ⋅
                              ◯E (M'' : ofe_car (gmapO _ _)))
      with "Hγ● Hγ◯") as "[$$]";
    [ apply excl_auth_update | ]; done.
  Qed.

End ghost_theory.


(* -------------------------------------------------------------------------- *)
(** Map interface. *)

(* We introduce a representation predicate [is_map'] defined on top of the
   predicate [is_map], which offered by the [MapLib] API. The difference
   between [is_map'] and [is_map] is that [is_map'] relates values to maps of
   type [nat → bool], whereas [is_map] relates values to maps of type
   [val → val]. *)

Section map_lib.
  Context `{!heapGS Σ}.
  Context `{!MapLib Σ}.
  Implicit Types M : gmap nat bool.

  Definition is_map' m M :=
    (∃ (M' : gmap val val),
      is_map m M'                                                   ∗
      ⌜ map_Forall (λ (i : nat) (b : bool), M' !! #i = Some #b) M ⌝ ∗
      ⌜ ∀ (i : nat), M !! i = None → M' !! #i = None              ⌝)%I.

  Lemma make_map_spec' Ψ1 Ψ2 :
    ⊢ EWP make_map #() <| Ψ1 |> {| Ψ2 |} {{ m, is_map' m ∅ }}.
  Proof.
    iApply ewp_pers_mono. { by iApply make_map_spec. }
    iIntros "!#" (m) "Hm". iModIntro. iExists ∅. by iFrame.
  Qed.

  Lemma map_lookup_spec' Ψ1 Ψ2 M m (i : nat) :
    is_map' m M -∗
      EWP map_lookup m #i <| Ψ1 |> {| Ψ2 |} {{ y,
        is_map' m M  ∗
        ⌜ (match M !! i with
           | Some b => y = SOMEV #(b : bool)
           | None   => y = NONEV
           end) ⌝
      }}.
  Proof.
    iIntros "Hm". iDestruct "Hm" as (M') "(Hm & % & %)".
    iApply (ewp_pers_mono _ _ with "[Hm]").
    { by iApply (map_lookup_spec with "Hm"). }
    iIntros "!#" (b) "[Hm %]". iModIntro.
    iSplit; [iExists M'; by iFrame|].
    iPureIntro. case_eq (M !! i).
    { intros b' Hlkup. revert H1. by rewrite (H _ _ Hlkup). }
    { intro Hlkup. revert H1. by rewrite (H0 _ Hlkup). }
  Qed.

  Lemma map_insert_spec' Ψ1 Ψ2 M m (i : nat) (b : bool) :
    is_map' m M -∗
      EWP map_insert m #i #b <| Ψ1 |> {| Ψ2 |} {{ _,
        is_map' m (<[i := b]> M) }}.
  Proof.
    iIntros "Hm". iDestruct "Hm" as (M') "(Hm & % & %)".
    iApply (ewp_pers_mono with "[Hm]").
    { by iApply (map_insert_spec with "Hm"). }
    iIntros "!#" (_) "Hm". iModIntro. iExists (<[#i:=#b]> M'). iFrame.
    iPureIntro. split; intro j; [intro b'|];
    case (Nat.eq_dec i j) as [->|];
    try (rewrite !lookup_insert; by inversion 1).
    { rewrite !lookup_insert_ne; [apply H| |done]; intro Heq; apply n;
      inversion Heq; by apply Znat.Nat2Z.inj.
    }
    { rewrite !lookup_insert_ne; [apply H0| |done]; intro Heq; apply n;
      inversion Heq; by apply Znat.Nat2Z.inj.
    }
  Qed.

  Lemma map_delete_spec' Ψ1 Ψ2 M m (i : nat) :
    is_map' m M -∗
      EWP map_delete m #i <| Ψ1 |> {| Ψ2 |} {{ _,
        is_map' m (delete i M) }}.
  Proof.
    iIntros "Hm". iDestruct "Hm" as (M') "(Hm & % & %)".
    iApply (ewp_pers_mono with "[Hm]").
    { by iApply (map_delete_spec with "Hm"). }
    iIntros "!#" (_) "Hm". iModIntro. iExists (delete #i M'). iFrame.
    iPureIntro. split; intro j; [intro b'|];
    case (Nat.eq_dec i j) as [->|]; try (rewrite !lookup_delete; by inversion 1).
    { rewrite !lookup_delete_ne; [apply H| |done]; intro Heq; apply n;
      inversion Heq; by apply Znat.Nat2Z.inj.  }
    { rewrite !lookup_delete_ne; [apply H0| |done]; intro Heq; apply n;
      inversion Heq; by apply Znat.Nat2Z.inj.
    }
  Qed.

End map_lib.


(* -------------------------------------------------------------------------- *)
(** Rewriting principle for [LOOKUP]. *)

Lemma upcl_LOOKUP `{!heapGS Σ} sView v Φ :
  iEff_car (Σ:=Σ) (upcl MS (LOOKUP sView)) v Φ ≡
    (∃ M (i : nat), ⌜ v = #i ⌝ ∗ sView M ∗
      □ (∀ (b : bool),
          (⌜ default b (M !! i) = b ⌝ ∗ sView (<[i:=b]> M)) -∗ Φ #b))%I.
Proof. by rewrite /LOOKUP (pers_upcl_tele' [tele _ _] [tele _]). Qed.


(* -------------------------------------------------------------------------- *)
(** Verification of [interp]. *)

Section interp.
  Context `{!heapGS Σ}.

  Lemma interp_correct : interp_spec.
  Proof.
    iIntros (sView). iLöb as "IH". iIntros (M p) "HM".
    case p as [p1 p2|p1 p2|(s, i)]; unfold interp; simpl.
    - do 11 ewp_value_or_step. ewp_bind_rule.
      iApply (ewp_pers_mono with "[HM]"). { by iApply ("IH" with "HM"). }
      iIntros "!#" (y) "[%M' [%b (HM&%Heval&->)]] !>". simpl.
      destruct b as [|] eqn:?.
      + do 3 ewp_value_or_step.
        iApply (ewp_pers_mono with "[HM]"). { by iApply ("IH" with "HM"). }
        iIntros "!#" (y) "[%M'' [%b' (HM&%Heval'&->)]] !>". simpl.
        iExists (M' ∪ M''), b'. rewrite map_union_assoc. iFrame.
        by rewrite Heval' (eval_union _ M'' _ _ Heval) //.
      + ewp_pure_steps. iExists M', false. iFrame.
        by rewrite Heval.
    - do 11 ewp_value_or_step. ewp_bind_rule.
      iApply (ewp_pers_mono with "[HM]"). { by iApply ("IH" with "HM"). }
      iIntros "!#" (y) "[%M' [%b (HM&%Heval&->)]] !>". simpl.
      destruct b as [|] eqn:?.
      + ewp_pure_steps. iExists M', true. iFrame.
        by rewrite Heval.
      + do 3 ewp_value_or_step.
        iApply (ewp_pers_mono with "[HM]"). { by iApply ("IH" with "HM"). }
        iIntros "!#" (y) "[%M'' [%b' (HM&%Heval'&->)]] !>". simpl.
        iExists (M' ∪ M''), b'. rewrite map_union_assoc. iFrame.
        by rewrite Heval' (eval_union _ M'' _ _ Heval) //.
    - ewp_pure_steps. iApply ewp_do_ms. rewrite upcl_LOOKUP.
      iExists M, i. iFrame. iSplit; [done|].
      iIntros "!#" (b) "[%Hb HM]". unfold interp_sign. ewp_pure_steps.
      assert (M ∪ {[i:=b]} = <[i:=b]> M) as Haux.
      { apply map_eq. intro j. case (Nat.eq_dec i j) as [->|].
        { by rewrite lookup_insert -Hb lookup_union lookup_singleton;
          case (M !! j). }
        { rewrite lookup_insert_ne; [|done].
          rewrite lookup_union lookup_singleton_ne; [|done].
          by case (M !! j).
        }
      }
      destruct s as [|] eqn:?; ewp_pure_steps.
      + iExists {[i:=b]}, b. rewrite Haux. iFrame.
        rewrite lookup_insert //. by case b.
      + done.
      + iExists {[i:=b]}, (negb b). rewrite Haux. iFrame.
        by rewrite lookup_insert //.
  Qed.

End interp.


(* -------------------------------------------------------------------------- *)
(** Verification of [satisfy]. *)

Section satisfy.
  Context `{!heapGS Σ, !MapLib Σ, !satG Σ}.

  Lemma satisfy_handler_correct p m γ :
    ∀ M,
      is_map' m M -∗
        blackboardSt γ M -∗
          deep_handler ⊤
             (* Handlee's protocol pair: *) ⊥ (LOOKUP (studentView γ))
            ((* Handlee's postcondition: *) λ y, ∃ M' b,
               studentView γ M' ∗
               ⌜ eval M' p = Some b ⌝ ∗ ⌜ y = #b ⌝
            )

            ((* Effect branch: *) λ: "i" "k",
               match: map_lookup m "i" with
                 SOME "b" =>
                   "k" "b"
               | NONE =>
                   (map_insert m "i" #true;; "k" #true)
                     ||
                   (map_insert m "i" #false;;
                      ("k" #false)
                        ||
                      (map_delete m "i";; #false)
                   )
               end
            )%V
            ((* Return branch: *) λ: "b", "b")%V 

            ⊥ ⊥
            ((* Handler's postcondition: *) λ b,
              (⌜ b = #false ⌝ -∗
                 (is_map' m M ∗ blackboardSt γ M ∗ studentView γ M) ) ∗
               ⌜ b = #(bool_decide (satisfiable' M p)) ⌝).
  Proof.
    iIntros (M) "Hm HM". iLöb as "IH" forall (M).
    rewrite deep_handler_unfold.
    iSplit; [|iSplit]; [|by iIntros (??) "HFalse"; rewrite upcl_bottom|].
    - iIntros (y) "[%M' [%b (HstView & %Heval & ->)]]". ewp_pure_steps.
      iDestruct (check_blackboard with "HM HstView") as %<-.
      iFrame. iSplit; [done|]; iPureIntro. f_equal. destruct b.
      { by rewrite bool_decide_true; [|apply eval_true]. }
      { by rewrite bool_decide_false; [|apply eval_false]. }
    - iIntros (v k). rewrite upcl_LOOKUP.
      iIntros "[%M' [%i (-> & HstView & #Hk)]]". ewp_pure_steps.
      iDestruct (check_blackboard with "HM HstView") as %<-.
      iApply (ewp_bind' (CaseCtx _ _)). done.
      iApply (ewp_mono with "[Hm]").
      { by iApply (map_lookup_spec' with "Hm"). }
      iIntros (b) "[Hm %Hlkp] !>". simpl.
      destruct (M !! i) as [b'|] eqn:?.
      + rewrite Hlkp. ewp_pure_steps.
        iApply ("Hk" with "[HstView]").
        { rewrite insert_id; [|done]. by iFrame. }
        iNext. by iApply ("IH" with "Hm HM").
      + rewrite Hlkp. ewp_pure_steps.
        iApply (ewp_bind [(IfCtx _ _); (AppRCtx _)]); first done.
        iApply (ewp_mono with "[Hm]").
        { by iApply (map_insert_spec' with "Hm"). }
        iIntros (?) "Hm !>". simpl. ewp_pure_steps.
        iApply fupd_ewp.
        iMod (erase_blackboard _ (<[i:=true]>M) with "HM HstView")
          as "[HM HstView]".
        iModIntro.
        iApply (ewp_mono with "[HstView HM Hm]").
        { iApply ("Hk" with "[HstView]"). { by iFrame. }
          by iApply ("IH" with "Hm HM").
        }
        iIntros (b') "[HM ->] !>".
        case_eq (satisfiable_dec' (<[i:=true]>M) p); intros Hsat _;
        ewp_pure_steps.
        { iSplit; [by iIntros "%"|]. iPureIntro.
          rewrite bool_decide_true; [done|].
          rewrite satisfiable_insert_iff'; [|apply Heqo].
          by exists true.
        }
        { iApply (ewp_bind' (AppRCtx _)); first done.
          iDestruct ("HM" with "[//]") as "(Hm & HM & HstView)".
          iApply (ewp_mono with "[Hm]").
          { by iApply (map_insert_spec' with "Hm"). }
          iIntros (?) "Hm". simpl.
          iMod (erase_blackboard _ (<[i:=false]>M) with "HM HstView")
            as "[HM HstView]".
          iModIntro. ewp_pure_steps. ewp_bind_rule.
          iApply (ewp_mono with "[HstView HM Hm]").
          { iApply ("Hk" with "[HstView]"). { by iFrame. }
            rewrite insert_insert. by iApply ("IH" with "Hm HM").
          }
          iIntros (b') "[Hb' ->] !>". simpl.
          case_eq (satisfiable_dec' (<[i:=false]>M) p); intros Hsat' _;
          ewp_pure_steps.
          { iSplit; [by iIntros "%"|]. iPureIntro.
            rewrite bool_decide_true; [done|].
            rewrite (satisfiable_insert_iff' _ _ _ Heqo).
            by exists false.
          }
          { iDestruct ("Hb'" with "[//]") as "(Hm & HM & HstView)".
            iApply fupd_ewp.
            iMod (erase_blackboard _ M with "[$] [$]") as "[HM HstView]".
            iModIntro.
            iApply (ewp_bind' (AppRCtx _)); first done. simpl.
            iApply (ewp_mono with "[Hm]").
            { by iApply (map_delete_spec' with "Hm"). }
            iIntros (y) "Hm !>". ewp_pure_steps.
            rewrite (delete_insert _ _ _ Heqo). iFrame. iPureIntro.
            split; [done|]. rewrite bool_decide_false; [done|].
            rewrite (satisfiable_insert_iff' _ _ _ Heqo). by intros [[|] ?].
          }
        }
  Qed.

  Lemma satisfy_correct p : satisfy_spec p.
  Proof.
    unfold satisfy_spec, satisfy. ewp_pure_steps. ewp_bind_rule.
    iApply ewp_pers_mono. { by iApply make_map_spec'. }
    iIntros "!#" (m) "Hm". simpl.
    iMod (introduce_blackboard (∅ : gmap natO boolO))
      as "[%γ [Hblackboard HstView]]". iModIntro.
    ewp_pure_steps.
    iApply (ewp_mono with "[Hm Hblackboard HstView]").
    - iApply (ewp_deep_try_with with "[HstView]").
      { iApply (ewp_pers_mono _ _ _ _ (λ y , ∃ M' b,
          studentView γ M' ∗ ⌜ eval M' p = Some b ⌝ ∗ ⌜ y = #b ⌝)
          with "[HstView]")%I.
        + by iApply (interp_correct with "HstView").
        + iIntros "!#" (m') "[%M' [%b (?&?&?)]] !>".
          iExists M', b. rewrite map_union_comm;
          last by apply map_disjoint_empty_l.
          rewrite map_union_empty. by iFrame.
      }
      { by iApply (satisfy_handler_correct with "Hm Hblackboard"). }
    - iIntros (y) "[H ->] !>". iPureIntro.
      unfold satisfiable. 
      by case (satisfiable_dec' ∅ p); intros Hsat; [
      rewrite !bool_decide_true | rewrite !bool_decide_false ].
  Qed.

End satisfy.
