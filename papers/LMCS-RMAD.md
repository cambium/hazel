Verifying an Effect-Handler-Based Define-By-Run Reverse-Mode AD Library
=======================================================================

This document is a guide to the Coq formalization of the paper
[*Verifying an Effect-Handler-Based Define-By-Run Reverse-Mode AD Library*](http://cambium.inria.fr/~fpottier/publis/de-vilhena-pottier-verifying-rmad.pdf).


## Relevant files

There are three files in the repository that are relevant to the paper:

1. The OCaml interface of the library [ad.mli](../src/ad.mli).

2. The OCaml implementation of the library [ad.ml](../src/ad.ml).

3. The Coq formalization of the results in the paper [automatic_differentiation.v](../theories/case_studies/automatic_differentiation.v).

## Link between paper and formalization


### Main result

The main result of the paper, Statement 5.1, is Definition `diff_spec` in the formalization.
The proof of this statement corresponds to Theorem `diff_correct`.


### Notation

Here is how the paper notation compares to the notation introduced in Coq:

|                                              | Paper                     | Coq formalization                     |
|----------------------------------------------|---------------------------|---------------------------------------|
| Definition 6.1 (Expressions)                 | `Exp`<sub>`I`</sub>       | `Expr I`                              |
| Definition 6.2 (Expression Evaluation)       | `〚E〛`<sub>`ρ`</sub>      | `eval (emap ρ E)`                     |
| Definition 6.3 (Bindings; contexts)          | `let u = a op b`          | `(u, (a, op, b))`                     |
| Definition 6.4 (Filling)                     | `K[y]`                    | `Let K .in y` or `filling K y`        |
| Definition 6.5 (Extension of an Environment) | `ρ{K}`                    | `ρ.{[K]}` or `extension ρ K`          |
| Definition 6.6 (Partial Derivative)          | `∂ E / ∂ j`               | `∂ E ./ ∂ j  .at (λ i, Leaf i)`       |
| Definition 6.7 (Derivative)                  | `E'`                      | `∂ E ./ ∂ tt .at (λ _, Xₑ)`           |
| Definition 8.2 (Runtime Repr. of an Expr.)   | `e isExp E`               | `isExp e E`                           |
| Definition 8.3 (Runtime Repr. of a Semiring) | `isDict`                  | `isDict`                              |
| Definition 8.7 (Forward Invariant)           | `ForwardInv K`            | `forward_inv K`                       |
| Definition 8.8 (Backward Invariant)          | `BackwardInvBody K₁ K₂ y` | `backward_inv K₁ K₂ y`                |
| Definition 8.9 (Abstract View of Vertices)   | `u isSubExpRaw E`         | `isSubExpRaw u E`                     |
|                                              | `u isSubExp E`            | `isSubExp u E`                        |
| Definition 8.10 (Protocol)                   | `OP`                      | `OP`                                  |
